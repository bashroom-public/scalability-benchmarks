# Scalability Benchmarks

## Table of content

[[_TOC_]]

## Install

```bash
npm ci
```

## Serve APIs

```bash
./scripts/serve/index.bash
```

## Develop APIs

### Execute the default API

```bash
npm run dev:default
```

### Run an API

```bash
npm run dev -- ${benchmarkPath}
# Example: npm run dev -- benchmarks/image-to-words-or-expressions/self-vs-openai/index.ts
```

## Run a benchmark

```bash
./scripts/benchmark/index.bash
```

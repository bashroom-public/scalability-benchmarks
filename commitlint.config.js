module.exports = {
	extends: ['@commitlint/config-conventional'],
	rules: {
		'header-max-length': [0, 'always', 4000],
		'type-enum': [2, 'always', ['build', 'chore', 'ci', 'docs', 'feat', 'fix', 'perf', 'refactor', 'revert', 'style', 'test', 'wip']],
	},
};

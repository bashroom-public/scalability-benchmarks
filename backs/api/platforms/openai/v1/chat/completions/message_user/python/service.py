import json
import requests

def execute(requestPayload):
	try:
		headers = {
			"Content-Type": "application/json",
			"Authorization": f"Bearer {requestPayload.apiKey}"
		}

		payload = {
			"model": requestPayload.model,
			"messages": [
				{
					"role": "user",
					"content": requestPayload.content
				}
			],
			"max_tokens": requestPayload.maxTokens
		}

		response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload).json()

		return response
	except Exception as e:
		print(f"Error: {e}")
		return []

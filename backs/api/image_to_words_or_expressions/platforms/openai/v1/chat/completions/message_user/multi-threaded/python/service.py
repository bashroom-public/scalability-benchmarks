import json
import requests
import threading

from backs.api.platforms.openai.v1.chat.completions.message_user.python.service import execute as backsApiPlatformsOpenaiV1ChatCompletionsMessageUserPythonExecute

def execute(requestPayload):
	try:
		responseList = []
		imageList = json.loads(requestPayload.imageListString)

		def process_image(imageItem):
			subRequestPayload = {
				"apiKey": requestPayload.apiKey,
				"model": requestPayload.model,
				"content": [
					{
						"type": "text",
						"text": requestPayload.prompt
					},
					{
						"type": "image_url",
						"image_url": {
							"url": f"data:image/jpeg;base64,{imageItem}"
						}
					}
				],
				"maxTokens": requestPayload.maxTokens
			}
			response = backsApiPlatformsOpenaiV1ChatCompletionsMessageUserPythonExecute(subRequestPayload)
			responseList.append(response)

		threads = []
		for imageItem in imageList:
			thread = threading.Thread(target=process_image, args=(imageItem,))
			threads.append(thread)
			thread.start()

		for thread in threads:
			thread.join()

		return responseList
	except Exception as e:
		print(f"Error: {e}")
		return []

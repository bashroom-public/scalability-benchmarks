import logging

from flask import request
from flask_restx import Resource

from .api import api
from .service import execute as execute_service

log = logging.getLogger(__name__)

ns = api.namespace('execute', description='Generate words or expressions from image through OpenAI')

@ns.route('/backs/api/image-to-words-or-expressions/platforms/openai/v1/chat/completions/message_user/mimd/python')
class Openai(Resource):
	@api.response(200, 'Success')
	@api.response(400, 'Validation Error')
	def post(self):
		"""
		Generate words or expressions from image
		"""
		return execute_service(request.json)

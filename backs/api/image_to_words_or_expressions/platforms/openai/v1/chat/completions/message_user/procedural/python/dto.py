from flask_restx import fields

class execute:
	apiKey = fields.String(
		required=True,
		description='The API key for authorization',
		example='12345abcdeABCDE12345',
		default='YourAPIKeyHere'
	)
	model = fields.String(
		required=True,
		description='The model to be used for processing',
		example='text-davinci-002',
		default='text-davinci-002'
	)
	content = fields.String(
		required=True,
		description='The content to use',
		example='What is the meaning of life?',
		default='What is the meaning of life?'
	)

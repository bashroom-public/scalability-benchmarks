import json
import requests

from backs.api.platforms.openai.v1.chat.completions.message_user.python.service import execute as backsApiPlatformsOpenaiV1ChatCompletionsMessageUserPythonExecute

def execute(requestPayload):
	try:
		responseList = []
		imageList = json.loads(requestPayload.imageListString)

		for imageItem in imageList:
			subRequestPayload = {
				"apiKey": requestPayload.apiKey,
				"model": requestPayload.model,
				"content": [
					{
						"type": "text",
						"text": requestPayload.prompt
					},
					{
						"type": "image_url",
						"image_url": {
							"url": f"data:image/jpeg;base64,{imageItem}"
						}
					}
				],
				"maxTokens": requestPayload.maxTokens
			}
			response = backsApiPlatformsOpenaiV1ChatCompletionsMessageUserPythonExecute(subRequestPayload)
			responseList.append(response)

		return responseList
	except Exception as e:
		print(f"Error: {e}")
		return []

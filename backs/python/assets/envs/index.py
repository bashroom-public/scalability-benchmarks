API_DESCRIPTION = 'Flask API to get words or expressions from images through OpenAI API'
API_TITLE = 'image_to_words_or_expressions_openai_flask_api'
API_VERSION = '1.0.0'
FLASK_SERVER_NAME = 'localhost:3000'
FLASK_DEBUG = True
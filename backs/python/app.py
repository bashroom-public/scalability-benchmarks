import logging.config
import os

from flask import Blueprint, Flask
from flask_restx import api

from backs.api.image_to_words_or_expressions.platforms.openai.v1.chat.completions.message_user.simd.python.api import api
from backs.api.image_to_words_or_expressions.platforms.openai.v1.chat.completions.message_user.simd.python.controller import ns as backsApiImageToWordsOrExpressionsPlatformsOpenaiV1ChatCompletionsMessageUserSimdPython

from backs.api.platforms.openai.v1.chat.completions.message_user.python.api import api
from backs.api.platforms.openai.v1.chat.completions.message_user.python.controller import ns as backsApiPlatformsOpenaiV1ChatCompletionsMessageUserPython

from backs.python.assets.envs import index as envs

app = Flask(__name__)
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), './logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)

def configure_app(flask_app):
	flask_app.config['SERVER_NAME'] = envs.FLASK_SERVER_NAME

def initialize_app(flask_app):
	configure_app(flask_app)
	blueprint = Blueprint('api', __name__, url_prefix='/api')
	api.init_app(blueprint)

	api.add_namespace(backsApiImageToWordsOrExpressionsPlatformsOpenaiV1ChatCompletionsMessageUserSimdPython)
	api.add_namespace(backsApiPlatformsOpenaiV1ChatCompletionsMessageUserPython)

	flask_app.register_blueprint(blueprint)

def main():
	initialize_app(app)
	log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
	app.run(debug=envs.FLASK_DEBUG)

if __name__ == "__main__":
	main()

#!/usr/bin/env bash

repoDir=$(git rev-parse --show-toplevel)

npm run build

pip3 install --requirement requirements.txt

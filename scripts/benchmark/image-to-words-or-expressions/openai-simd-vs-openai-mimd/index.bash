#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)

apiKey=${1:-${API_KEY:-""}}
prompt=${2:-${PROMPT:-"Activate a multi-faceted and nuanced analysis protocol on the provided image, aimed at extracting a diverse set of descriptors. Follow these structured steps:\n\n1. Identification: Isolate key visual elements, themes, or components within the image. Prioritize those that are most prominent or unique.\n\n2. Categorization: Classify each identified element into descriptive categories (e.g., objects, emotions, colors, textures, actions).\n\n3. Evaluation: Assign a 'consistence' value to each descriptor. This value, ranging between 0 and 1, quantifies the descriptor's relevance and prominence in the image. Ensure this evaluation is objective and data-driven.\n\n4. Synthesis: Compile the descriptors into a JSON array named 'descriptionArray'. Each element of the array should be a JSON object with two keys: 'wordOrExpression' and 'consistence'. The 'wordOrExpression' key will contain a string descriptor from step 2, and the 'consistence' key will hold its corresponding value from step 3.\n\n5. Diversity Assurance: Confirm that each descriptor adds a unique dimension to the overall interpretation of the image, ensuring a comprehensive and holistic understanding.\n\nOutput Specification: The final output should exclusively be the 'descriptionArray' JSON array, containing at least 10 elements, without any introductory or concluding text."}}
endpoint=${3:-${ENDPOINT:-"http://localhost:3000/backs/api/image-to-words-or-expressions/platforms/openai/v1/chat/completions/message_user/simd/python"}}
directoryPathList=${4:-${DIRECTORY_PATH_LIST:-"./data/size-1 ./data/size-2"}}
modelList=${4:-${MODEL_LIST:-"gpt-4-vision-preview"}}
maxTokensList=${5:-${MAX_TOKENS_LIST:-"300 600 1000 4096"}}

pidList=""
rc=0

cd "${repoDir}"

indexJsPath="dist/benchmarks/image-to-words-or-expressions/index.js"

# Check if kubectl binary is available in .cache dir
if [ ! -f "${repoDir}/.cache/kubectl" ]; then
    mkdir -p "${repoDir}/.cache"
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    mv kubectl "${repoDir}/.cache/kubectl"
    chmod +x "${repoDir}/.cache/kubectl"
fi

# Alias kubectl to the binary in .cache dir
alias kubectl="${repoDir}/.cache/kubectl"

# Add kubectl to the path
export PATH="${repoDir}/.cache:${PATH}"

# Create configmap for the benchmark script
kubectl create configmap benchmark-script -n scalability-benchmarks --from-file="${indexJsPath}"

create_benchmark_ressources() {

benchmarkName="$1"
benchmarkConfigs="$2"
# Create pod
cat<<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: ${benchmarkName}
  namespace: scalability-benchmarks
spec:
  imagePullSecrets:
    - name: scalability-benchmarks-registry-pull-secret
  containers:
  - name: python-backs
    image: registry.gitlab.com/bashroom-public/scalability-benchmarks/scalability-benchmark:latest
  - name: benchmark
    image: loadimpact/k6:0.34.0
    command: ["run", "index.js", "--summary-export=/output/${benchmarkName}.json"]
    env:
    - name: BENCHMARK_CONFIGS
      value: |
        ${benchmarkConfigs}
    volumeMounts:
    - name: benchmark-script
      mountPath: /
    - name: output-volume
      mountPath: /output
  - name: keep-alive-container
    image: busybox
    command: ["/bin/sh", "-c", "tail -f /dev/null"]
    volumeMounts:
    - name: output-volume
      mountPath: /output
  volumes:
  - name: benchmark-script
    configMap:
      name: benchmark-script
  - name: output-volume
    emptyDir: {}
EOF
}

for directoryPathItem in ${directoryPathList}; do
    # Dasherize the directory path and remove leading ./ if any
    directoryPathItemDasherized=$(echo "${directoryPathItem}" | sed -e 's/^\.\///' -e 's/\//\-/g')

    for modelItem in ${modelList}; do
        for maxTokensItem in ${maxTokensList}; do
            echo "directoryPathItem=${directoryPathItem}"
            echo "modelItem=${modelItem}"
            echo "maxTokensItem=${maxTokensItem}"

            benchmarkName="benchmark-${directoryPathItemDasherized}-${modelItem}-${maxTokensItem}"
            benchmarkConfigs='{"apiKey": "'${apiKey}'", "directoryPath": "'${directoryPathItem}'", "endpoint": "'${endpoint}'", "maxTokens": "'${maxTokensItem}'", "model": "'${modelItem}'", "prompt": "'${prompt}'"}'

            create_benchmark_ressources "${benchmarkName}" "${benchmarkConfigs}"
        done
    done
done

# Small sleep to let the pods start
sleep 5

# For each pod, wait until the k6 process is done and get the output file and put it in the artifacts folder
for podName in $(kubectl get pods -n scalability-benchmarks -o jsonpath='{.items[*].metadata.name}' | grep benchmark); do
    echo "podName=${podName}"
    {
        kubectl wait --for=condition=Ready pod/${podName} -n scalability-benchmarks --timeout=10m
        # Wait for the k6 process to be done
        kubectl exec -n scalability-benchmarks "${podName}" -c benchmark -- /bin/sh -c 'while [ "$(ps -ef | grep k6 | grep -v grep)" ]; do sleep 1; done'
        # Get the output file
        kubectl cp scalability-benchmarks/${podName}:/output/* "${repoDir}/artifacts/" -c benchmark
        echo "Benchmark ${podName} done"
    } &
    pidList="${pidList} $!"
done


for pidItem in ${pidList}; do
    wait ${pidItem} || let "rc=1"
done

exit ${rc}

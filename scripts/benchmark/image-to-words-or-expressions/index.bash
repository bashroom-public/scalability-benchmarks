#!/usr/bin/env bash

set -eE

repoDir=$(git rev-parse --show-toplevel)

areAllBenchmarksWanted=${1:-${ARE_ALL_BENCHMARKS_WANTED:-"false"}}

pidList=""
rc=0

cd "${repoDir}"

if [ "${areAllBenchmarksWanted}X" = "falseX" ]; then
	benchmarkList="self-vs-openai-2/index.bash"
else
	benchmarkList=$(find "${repoDir}/scripts/benchmark/image-to-words-or-expressions" -type f -name "*.bash")
fi


for benchmarkItem in ${benchmarkList}; do
	bash "${benchmarkItem}" &
	pidItem=$!
	pidList="${pidList} ${pidItem}"
done


for pidItem in ${pidList}; do
	wait ${pidItem} || let "rc=1"
done

exit ${rc}

import { group, check } from 'k6';
import http, { FileData, file } from 'k6/http';
import { readFile, readdir } from 'fs/promises';

export interface BenchmarkConfigs {
	apiKey: string;
	directoryPath: string;
	endpoint: string;
	maxTokens: string;
	model: string;
	prompt: string;
}

export const benchmark = async (configs: BenchmarkConfigs) => {
	const { apiKey, prompt, directoryPath, endpoint, maxTokens, model } = configs;
	const imagePathArray = await readdir(directoryPath);
	const imageBase64Array = await Promise.all(imagePathArray.map(async (imagePath) => {
		const image = await readFile(imagePath);
		const imageBase6Item = image.toString('base64');
		return imageBase6Item;
	}));

	group('Benchmarking Image Processing Function', function () {
		const response = http.post(endpoint, { apiKey, imageListString: JSON.stringify(imageBase64Array), maxTokens, model, prompt }, { headers: { 'Content-Type': 'application/json' } });

		check(response, {
			'is status 200': r => r.status === 200,
		});
	});
};

const configsString = __ENV.BENCHMARK_CONFIGS;

if (configsString === undefined || configsString === null) {
	throw new Error('Benchmark configuration not provided');
}else{
	const configs: BenchmarkConfigs = JSON.parse(configsString);
	benchmark(configs);
}
